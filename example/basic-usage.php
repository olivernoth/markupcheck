<?php

/*
 * Determine from where this file is being included and set certain paths for usage later on.
 */
$urlToExampleDir = '//' . $_SERVER['HTTP_HOST'] . '/example';
$autoloadFile = __DIR__ . '/../vendor/autoload.php';

if (false !== ($isVendor = strpos(__DIR__, 'vendor'))) {
    $urlToExampleDir = '//' . $_SERVER['HTTP_HOST'] . '/vendor/oliver-noth/markup-check/example';

    $autoloadFile = __DIR__ . '/../../../../vendor/autoload.php';
}

// Provide class loader
if (is_readable($autoloadFile)) {
    require_once $autoloadFile;
}

// Render content
require 'partials/content.php';
