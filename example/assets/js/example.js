(function($) {

    $(window).on('load', function() {

        // When clicking on a card header avoid showing its link address in browser address bar
        $('.card-header > p > a').on('click', function() {

            if (history.pushState) {
                history.pushState(null, null, null);
            }
        })

        // When clicking on the footer image container, add/remove css class 'active'
        // (see corresponding stylesheet 'example[.min].css')
        $('footer .img').on('click', function() {

            $(this).toggleClass('active');
        }).on('focusout', function() {

            $(this).removeClass('active');
        })

        // When focusing or leaving url input field, remove css class 'is-invalid'
        // (corresponds with bootstrap 4.4 and given markup defined in rendered-tags.php)
        $('#url-input').on('keydown focus active blur', function() {
            $(this).removeClass('is-invalid');
        })

        // Process form submit
        $('#get-tags-from').on('submit', function(event) {

            // Disable default submit behaviour
            event.preventDefault();

            let maybeUrl = $(this).find('#url-input').val();
            let regex = new RegExp('((http[s]{0,1}:\/\/|[\/]{2,})([\w]+:[\w]+@)*)*[^\.\w_-]+[\.]+(\.[\w_-]+)*(:[\d]{2,4}[\.]+[\w_-]+)*(\/[\w_-]+(\.[a-z]+)*)*[\/]*[?&=\w]*');

            // Validate given input
            if (0 < $(this).find('#url-input').val().length && regex.test(maybeUrl)) {

                // Send ajax post request
                $.ajax({
                    context: this,
                    method: 'post',
                    type: 'text',
                    data: $(this).serialize(),
                    beforeSend: function() {

                        $(this).find('.rounded-right').addClass('disabled');
                        $(this).find('input[type="url"]').prop('disabled', true);
                        $(this).find('button[type="submit"] span.fa-icon').addClass('d-none');
                        $(this).find('button[type="submit"] span.spinner-border').removeClass('d-none');
                    },
                    error: function() {

                        $(this).find('.rounded-right').removeClass('disabled');
                        $(this).find('input[type="url"]').removeAttr('disabled');
                        $(this).find('button[type="submit"] span.spinner-border').addClass('d-none');
                        $(this).find('button[type="submit"] span.fa-icon').removeClass('d-none');
                    },
                    success: function(html) {

                        $(this).find('input[name="_csrf"]').replaceWith($(html).find('input[name="_csrf"]'));

                        $(this).find('+ p.lead').slideUp().remove();
                        $(html).find('p.lead').insertAfter($(this));

                        $(this).closest('header').find('+ div').slideUp().remove();
                        $(html).find('p.lead + div').insertAfter($(this).closest('header'));
                    },
                    complete: function() {

                        $(this).find('input[type="url"]').removeClass('is-invalid');
                        $(this).find('.rounded-right').removeClass('disabled');
                        $(this).find('input[type="url"]').removeAttr('disabled');
                        $(this).find('button[type="submit"] span.spinner-border').addClass('d-none');
                        $(this).find('button[type="submit"] span.fa-icon').removeClass('d-none');
                    }
                })
            } else {

                // Show invalid tooltip by adding css class 'is-invalid'
                // (corresponds with bootstrap 4.4 and given markup defined in rendered-tags.php)
                $(this).find('input[type="url"]').addClass('is-invalid');
            }
        })
    })
})(jQuery)