#!/bin/bash
# Call this script from where you have composer created this package (composer create oliver-noth/markup-check).
# It will create an index.php which then can act as entrypoint in your docroot.
cat << PHP > index.php
<?php

require 'example/basic-usage.php';
PHP
echo ""
echo "File $PWD/index.php successfully written."