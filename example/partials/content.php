<?php
/** @var string $urlToExampleDir */

// Start session to access superglobal $_SESSION
session_start();

// Determine, if current request is ajax
$isAjax = !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && 'xmlhttprequest' === strtolower($_SERVER['HTTP_X_REQUESTED_WITH']);
?>

<?php if (!$isAjax): ?>

    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>MarkupCheck</title>
        <link rel="icon" href="<?= $urlToExampleDir . '/assets/images/pngwing.com.robin.favicon.png?v=' . time() ?>"
              sizes="32x32">
        <link rel="icon" href="<?= $urlToExampleDir . '/assets/images/pngwing.com.robin.favicon.png?v=' . time() ?>"
              sizes="192x192">
        <link rel="apple-touch-icon-precomposed"
              href="<?= $urlToExampleDir . '/assets/images/pngwing.com.robin.favicon.png?v=' . time() ?>">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
              integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"
                integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
                crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
                integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
                crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
                integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
                crossorigin="anonymous"></script>
        <link rel="stylesheet" href="<?= $urlToExampleDir . '/assets/css/example.min.css' ?>">
        <script src="<?= $urlToExampleDir . '/assets/js/example.min.js' ?>"></script>
    </head>
    <body>

    <?php /* Render header */ ?>
    <?php require 'header.php'; ?>

    <?php /* Render footer */ ?>
    <?php require 'footer.php'; ?>

    </body>
    </html>
<?php else:

    // Security check
    $postedCsrfToken = $_POST['_csrf'] ?? false;

    // Set url to get the parsed tags from
    $maybeUrlToGetTagsFrom = ($_SESSION['_csrf_token'] === $postedCsrfToken && isset($_POST['url-input'])) ?

        // Defend xss - All input is tainted ;-)
        htmlspecialchars(strip_tags($_POST['url-input'])) :
        false;

    // Process ajax request
    if ($maybeUrlToGetTagsFrom):

        // Create new MarkupCheck
        $markupChecker = (new \OliverNoth\MarkupCheck\Main\Factory())->createHtmlMarkupChecker($maybeUrlToGetTagsFrom);

        // Get parsed tags
        $parsedTags = $markupChecker->getParsedTags();

        // Send response to calling ajax
        require 'rendered-tags.php';
    endif;
endif;
