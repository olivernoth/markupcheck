<?php
/** @var \OliverNoth\MarkupCheck\Html\Director $markupChecker */

/** @var array $parsedTags */

// Renew csrf token
$_SESSION['_csrf_token'] = base64_encode(random_bytes(32));

$typeCount = 0;
?>
<div>
    <?php /* Replaces existing hidden input initialized in header.php (see also assets/js/example[.min].js) */ ?>
    <input type="hidden" name="_csrf" value="<?= $_SESSION['_csrf_token'] ?>">

    <p class="lead">Parsed results for: '<?= htmlentities($markupChecker->getLabel()) ?>'</p>
    <div class="text-white bg-info mt-0">
        <div id="accordianId" class="accordion bg-info" role="tablist" aria-multiselectable="true">
            <?php
            foreach ($parsedTags as $type => $contents): ?>
                <div class="card rounded-0 bg-white text-info">
                    <div class="card-header bg-white text-info" role="tab" id="<?= $type ?>HeaderId">
                        <p class="mb-0">
                            <a class="btn btn-block btn-lg bg-info text-white text-left <?= 1 < ++$typeCount ? 'collapsed ' : '' ?>"
                               data-toggle="collapse" data-parent="#accordianId" href="#<?= $type ?>ContentId"
                               aria-expanded="true" aria-controls="<?= $type ?>ContentId">
                                <?= !empty($contents['tags']) ? count($contents['tags']) : count(
                                    $contents['notes']
                                ) ?> <?= $type ?>
                                <i class="fa fa-chevron-circle-down pull-right" aria-hidden="true"></i>
                                <i class="fa fa-chevron-circle-up pull-right" aria-hidden="true"></i>
                            </a>
                        </p>
                    </div>
                    <div id="<?= $type ?>ContentId" class="collapse <?= 1 === $typeCount ? 'show ' : '' ?>" role="tabpanel"
                         aria-labelledby="<?= $type ?>HeaderId" data-parent="#accordianId">
                        <div class="card-body pl-5 pr-5">
                            <div class="row">
                                <?php
                                foreach ($contents['notes'] as $note): ?>
                                    <div class="col-12 text-left">
                                        <?= $note ?>
                                    </div>
                                <?php
                                endforeach; ?>
                            </div>

                            <?php
                            if (!empty($contents['tags'])): ?>
                                <div class="row">
                                    <?php
                                    foreach ($contents['tags'] as $tagIndex => $tag): ?>
                                        <div class="col-md-4 col-sm-6 col-12 my-3">
                                            <div class="h-100 py-3 px-3 px-md-5 px-sm-5 border border-info rounded">
                                                <div class="row">
                                                    <div class="col-12 px-md-0 px-sm-0 pb-2">
                                                        <span class="badge badge badge-pill badge-info"><?= $tagIndex + 1 ?></span>
                                                    </div>
                                                </div>

                                                <?php
                                                foreach (
                                                    array_filter(
                                                        $tag,
                                                        function($key) {
                                                            return 'attributes' !== $key;
                                                        },
                                                        ARRAY_FILTER_USE_KEY
                                                    ) as $key => $value
                                                ): ?>
                                                    <div class="row mb-md-0 mb-sm-0 mb-2">
                                                        <div class="col-lg-6 col-md-12 col-sm-6 col-12 text-left font-weight-bold">
                                                            <?= $key ?>:
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 col-sm-6 col-12 text-left">
                                                            <?= htmlentities(
                                                                \OliverNoth\MarkupCheck\Helper\Utils::excerpt($value, 25)
                                                            ) ?>
                                                        </div>
                                                    </div>
                                                <?php
                                                endforeach; ?>

                                                <?php
                                                foreach ($tag['attributes'] as $key => $value): ?>
                                                    <div class="row mb-md-0 mb-sm-0 mb-2">
                                                        <div class="col-lg-6 col-md-12 col-sm-6 col-12 text-left font-weight-bold">
                                                            <?= $key ?>:
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 col-sm-6 col-12 text-left">
                                                            <?= htmlentities(
                                                                \OliverNoth\MarkupCheck\Helper\Utils::excerpt($value, 25)
                                                            ) ?>
                                                        </div>
                                                    </div>
                                                <?php
                                                endforeach; ?>
                                            </div>
                                        </div>
                                    <?php
                                    endforeach; ?>
                                </div>
                            <?php
                            endif; ?>
                        </div>
                    </div>
                </div>
            <?php
            endforeach; ?>
        </div>
    </div>
</div>
