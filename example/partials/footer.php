<?php
/** @var string $urlToExampleDir */

?>

<footer>
    <div class="row mx-0">
        <div class="col-12 text-center text-info py-2 px-0">
            <span class="pr-3">&copy; <?= date('Y') ?> www.example.com</span>
            <span class="img bg-white border border-info rounded-circle" tabindex="1">
                <img src="<?= $urlToExampleDir . '/assets/images/pngwing.com.robin.png' ?>"
                     height="auto" width="30" alt="Robin Hood" loading="lazy"
                >
            </span>
        </div>
    </div>
</footer>
