# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.4] - 2021-11-01
### Fixed
- Fixed .gitlab-ci.yml: Stage unittest

## [1.2.3] - 2021-11-01
### Changed
- Raise supported php version to at least 8.0

## [1.2.2] - 2020-05-26
### Added
- Sample for basic usage: Defend XSS and CSRF

## [1.2.1] - 2020-05-24
### Added
- Sample for basic usage: Bash script for creating an index.php added

## [1.2.0] - 2020-05-23
### Changed
- Sample for basic usage complemented: Provide an url via form, get parsed tags via ajax call

### Added
- Sample for basic-usage: Partials added
  - content.php
  - header.php
  - rendered-tags.php
  - footer.php
- MIT License added

### Fixed
- Fix error on upper cased h-tags

## [1.1.2] - 2020-05-13
### Changed
- composer.json: folder/file exclusion readjusted
- GitLab CI config: Limit job creation/execution
- GitLab CI: bashscript for getting parsed tags refactored

### Added
- composer.json: Package behat/behat added
- Behat context testing implemented
- GitLab CI config: Job behattest added

### Fixed
- Unittest bootstrapping fixed

## [1.1.1] - 2020-05-13
### Changed
- composer.json: Exclude certain folders/files from being archived

### Added
- GitLab CI: Config added

## [1.1.0] - 2020-05-11
### Changed
- composer.json: Minimum required php version upgraded to 7.4

### Added
- README.md: Docker documentation added
 
### Fixed
- ChangeLog.md: Spelling fixed
- Sample file example/basic-usage: Fixed detecting url to asset files

## [1.0.11] - 2020-05-11
### Changed
- composer.json: Removed exclusion of folder 'tests'
- Sample file example/basic-usage refactored: Inline styles/javascript moved to asset files

### Added
- asset files located in example/assets/

### Removed
- file .gitattributes removed

## [1.0.10] - 2020-05-10
### Added
- file .gitattributes added

## [1.0.9] - 2020-05-10
### Fixed
- composer.json: Fixed excluding folder 'tests' from being downloaded

## [1.0.8] - 2020-05-10
### Changed
- composer.json: Excluding folder 'tests' from being downloaded

### Fixed
- Sample file example/basic-usage: Fixed detecting url to asset files

## [1.0.7] - 2020-05-10
### Fixed
- README.md

## [1.0.6] - 2020-05-10
### Fixed
- README.md

## [1.0.5] - 2020-05-10
### Fixed
- composer.json: Package name fixed

## [1.0.4] - 2020-05-10
### Changed
- package folder structure changed

## [1.0.3] - 2020-05-10
### Fixed
- composer.json: Package name fixed

## [1.0.2] - 2020-05-10
### Changed
- package folder structure changed

## [1.0.1] - 2020-05-10
### Changed
- README.md: Location changed
- ChangeLog.md: Location changed

## [1.0.0] - 2020-05-10
- Initial release

[1.2.4]: https://gitlab.com/olivernoth/markupcheck/-/tags/v1.2.4
[1.2.3]: https://gitlab.com/olivernoth/markupcheck/-/tags/v1.2.3
[1.2.2]: https://gitlab.com/olivernoth/markupcheck/-/tags/v1.2.2
[1.2.1]: https://gitlab.com/olivernoth/markupcheck/-/tags/v1.2.1
[1.2.0]: https://gitlab.com/olivernoth/markupcheck/-/tags/v1.2.0
[1.1.2]: https://gitlab.com/olivernoth/markupcheck/-/tags/v1.1.2
[1.1.1]: https://gitlab.com/olivernoth/markupcheck/-/tags/v1.1.1
[1.1.0]: https://gitlab.com/olivernoth/markupcheck/-/tags/v1.1.0
[1.0.11]: https://gitlab.com/olivernoth/markupcheck/-/tags/v1.0.11
[1.0.10]: https://gitlab.com/olivernoth/markupcheck/-/tags/v1.0.10
[1.0.9]: https://gitlab.com/olivernoth/markupcheck/-/tags/v1.0.9
[1.0.8]: https://gitlab.com/olivernoth/markupcheck/-/tags/v1.0.8
[1.0.7]: https://gitlab.com/olivernoth/markupcheck/-/tags/v1.0.7
[1.0.6]: https://gitlab.com/olivernoth/markupcheck/-/tags/v1.0.6
[1.0.5]: https://gitlab.com/olivernoth/markupcheck/-/tags/v1.0.5
[1.0.4]: https://gitlab.com/olivernoth/markupcheck/-/tags/v1.0.4
[1.0.3]: https://gitlab.com/olivernoth/markupcheck/-/tags/v1.0.3
[1.0.2]: https://gitlab.com/olivernoth/markupcheck/-/tags/v1.0.2
[1.0.1]: https://gitlab.com/olivernoth/markupcheck/-/tags/v1.0.1
[1.0.0]: https://gitlab.com/olivernoth/markupcheck/-/tags/v1.0.0
