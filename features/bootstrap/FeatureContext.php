<?php
declare(strict_types=1);

/**
 * Behat test context for OliverNoth\MarkupCheck\Html\Factory, OliverNoth\MarkupCheck\Html\Director and OliverNoth\MarkupCheck\Html\Document
 *   - Defines application features from the specific context
 *
 * @class FeatureContext
 * @author Oliver Noth <info@nothbetrieb.de>
 * @copyright 2020 Oliver Noth
 */

use Behat\Behat\Context\Context;
use OliverNoth\MarkupCheck\{Main\Factory as MarkupCheckerFactory,
    Html\Director as HtmlDirector,
    Html\Document as HtmlDocument
};
use PHPUnit\Framework\Assert as PHPUnitAssert;

/**
 * Class FeatureContext
 *
 * @since 1.1.2
 */
class FeatureContext implements Context
{
    /**
     * Holds markup checker factory, which can create a markup checker distance.
     *
     * @since 1.1.2
     */
    private ?MarkupCheckerFactory $markupCheckerFactory = null;

    /**
     * Holds markup checker html director, which orchestrates the markup check functionality.
     *
     * @since 1.1.2
     */
    private ?HtmlDirector $markupCheckerHtmlDirector = null;

    /**
     * Initializes context.
     *
     * @since 1.1.2
     */
    public function __construct()
    {
        $this->markupCheckerFactory = new MarkupCheckerFactory();
    }

    /**
     * @Given there is an url :arg1
     * @param string $arg1
     * @since 1.1.2
     */
    public function thereIsAnUrl(string $arg1): void
    {
        $this->markupCheckerHtmlDirector = $this->markupCheckerFactory->createHtmlMarkupChecker($arg1);

        PHPUnitAssert::assertInstanceOf(HtmlDirector::class, $this->markupCheckerHtmlDirector);
    }

    /**
     * @When I check the html tags from :arg1
     * @param string $arg1
     * @since 1.1.2
     * @throws ReflectionException
     */
    public function iCheckTheHtmlTagsFrom(string $arg1): void
    {
        $reflection = new ReflectionClass($this->markupCheckerHtmlDirector);
        $htmlDocumentProperty = $reflection->getProperty('htmlDocument');
        $htmlDocumentProperty->setAccessible(true);

        $markupCheckerHtmlDocument = $htmlDocumentProperty->getValue($this->markupCheckerHtmlDirector);

        PHPUnitAssert::assertInstanceOf(HtmlDocument::class, $markupCheckerHtmlDocument);
    }

    /**
     * @Then I should get a non-empty array with headings, anchors and images
     * @since 1.1.2
     */
    public function iShouldGetANonEmptyArrayWithHeadingsAnchorsAndImages(): void
    {
        $parsedTags = $this->markupCheckerHtmlDirector->getParsedTags();

        PHPUnitAssert::assertIsArray($parsedTags);

        foreach (['headings', 'anchors', 'images'] as $key) {
            PHPUnitAssert::assertArrayHasKey($key, $parsedTags);
        }
    }
}
