Feature: Get parsed tags from an arbitrary url
  In order to check html tags from an arbitrary url
  As a tester
  I need to be able to receive parsed html tags from this url

  Rules:
  - url is well-formed

  Scenario: url starts with the scheme "https://"
    Given there is an url "https://getcomposer.org/"
    When I check the html tags from "https://getcomposer.org/"
    Then I should get a non-empty array with headings, anchors and images

  Scenario: url is not schematised
    Given there is an url "getcomposer.org"
    When I check the html tags from "getcomposer.org"
    Then I should get a non-empty array with headings, anchors and images
