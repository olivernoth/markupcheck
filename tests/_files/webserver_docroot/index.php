<?php declare(strict_types=1);
/*
 * This file is part of tests.
 * It acts as root file for the php built-in web server.
 *
 * (c) Oliver Noth <info@nothbetrieb.de>
 */

/*
 * $_ENV['VALID_MARKUP'] has been set in tests/bootstrap.php.
 */
defined('VALID_MARKUP') || define('VALID_MARKUP', base64_decode($_ENV['VALID_MARKUP']));

echo VALID_MARKUP;