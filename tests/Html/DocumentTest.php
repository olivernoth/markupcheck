<?php
declare(strict_types=1);
/**
 * Test case for OliverNoth\MarkupCheck\Html\HTMLDocument
 *   - Contains methods for testing the behaviour of the tested class
 *
 * @class HTMLDocumentTest
 * @author Oliver Noth <info@nothbetrieb.de>
 * @copyright 2020 Oliver Noth
 */

namespace OliverNoth\MarkupCheck\Tests\Html;

use \PHPUnit\Framework\TestCase;
use \OliverNoth\MarkupCheck\Html\Document as HtmlDocument;

/**
 * Class HTMLDocumentTest
 *
 * @coversDefaultClass \OliverNoth\MarkupCheck\Html\Document
 * @group document
 * @package OliverNoth\MarkupCheck\Tests\Html
 * @since 1.0.0
 */
final class DocumentTest extends TestCase
{
    /**
     * Provides data for testing HtmlDocument::__construct().
     *
     * @return array[]
     * @see \OliverNoth\MarkupCheck\Tests\Html\DocumentTest::testCanBeCreatedFromConstructorparams()
     * @since 1.0.0
     */
    public function constructorParamProvider(): array
    {
        return [
            [VALID_MARKUP, false],
            ['', true],
            [null, true],
            [VALID_URL, true],
            [INVALID_URL, true],
            [INVALID_MARKUP, true],
        ];
    }

    /**
     * Provides data for testing HtmlDocument::containsHtmlMarkup().
     *
     * @return array[]
     * @see \OliverNoth\MarkupCheck\Tests\Html\DocumentTest::testContainsHtmlMarkup()
     * @since 1.0.0
     */
    public function containsHtmlMarkupProvider(): array
    {
        return [
            ['', false, true],
            [null, false, true],
            [VALID_URL, false, true],
            [INVALID_URL, false, true],
            [VALID_MARKUP, true, false],
            [INVALID_MARKUP, false, true],
        ];
    }

    /**
     * Provides data for testing HtmlDocument::getDOMDocument().
     *
     * @return array[]
     * @see \OliverNoth\MarkupCheck\Tests\Html\DocumentTest::testGetDOMDocumentThrowsException()
     * @since 1.0.0
     */
    public function getDOMDocumentThrowsExceptionParamProvider(): array
    {
        return [
            ['', 'InvalidArgumentException', 'Empty string provided'],
            [null, 'InvalidArgumentException', 'Empty string provided'],
            [INVALID_MARKUP, 'InvalidArgumentException', 'Invalid markup provided'],
            [VALID_URL, 'InvalidArgumentException', 'Invalid markup provided'],
            [INVALID_URL, 'InvalidArgumentException', 'Invalid markup provided'],
        ];
    }

    /**
     * Provides data for testing HtmlDocument::getDOMElementAsArray().
     *
     * @return array[]
     * @see \OliverNoth\MarkupCheck\Tests\Html\DocumentTest::testGetDOMElementAsArray()
     * @since 1.0.0
     */
    public function getDOMElementAsArrayParamProvider(): array
    {
        $domElementAnchor = (new \DOMDocument())->createElement('a', VALID_URL);
        $domElementAnchor->setAttribute('href', VALID_URL);
        $domElementAnchor->setAttribute('target', '_blank');

        $domElementImage = (new \DOMDocument())->createElement('img', VALID_URL);
        $domElementImage->setAttribute('src', VALID_URL);
        $domElementImage->setAttribute('class', 'image');

        $domElementSpan = (new \DOMDocument())->createElement('span', WEB_SERVER_HOST);
        $domElementSpan->setAttribute('class', 'block');

        return [
            [
                $domElementAnchor,
                ['name' => 'a', 'value' => VALID_URL, 'attributes' => ['href' => VALID_URL, 'target' => '_blank']],
            ],
            [
                $domElementImage,
                ['name' => 'img', 'value' => VALID_URL, 'attributes' => ['src' => VALID_URL, 'class' => 'image']],
            ],
            [$domElementSpan, ['name' => 'span', 'value' => WEB_SERVER_HOST, 'attributes' => ['class' => 'block']]],
        ];
    }

    /**
     * Provides data for testing HtmlDocument::getDOMElementAsArray().
     *
     * @return array[]
     * @see \OliverNoth\MarkupCheck\Tests\Html\DocumentTest::testGetDOMElementAsArrayErrorOccurs()
     * @since 1.0.0
     */
    public function getDOMElementAsArrayErrorOccursParamProvider(): array
    {
        return [
            [''],
            [null],
        ];
    }

    /**
     * Provides data for testing HtmlDocument::getElementsByTagName().
     *
     * @return array[]
     * @see \OliverNoth\MarkupCheck\Tests\Html\DocumentTest::testGetElementsByTagName()
     * @since 1.0.0
     */
    public function getElementsByTagNameParamProvider(): array
    {
        return [
            ['', []],
            ['some text', []],
            [null, []],
            ['h1', [['name' => 'h1', 'rank' => 1, 'value' => 'H1 Headline', 'attributes' => []]]],
            ['h2', [['name' => 'h2', 'rank' => 2, 'value' => '<span>H2 Headline</span>', 'attributes' => []]]],
            [
                'a',
                [
                    [
                        'name' => 'a',
                        'value' => '<img src="//www.example.com/user.png" alt="User image">',
                        'attributes' => ['href' => 'mailto:user@example.com'],
                    ],
                ],
            ],
            [
                'img',
                [
                    [
                        'name' => 'img',
                        'value' => '',
                        'attributes' => ['src' => '//www.example.com/user.png', 'alt' => 'User image'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Provides data for testing HtmlDocument::getHeadings().
     *
     * @return array[]
     * @see \OliverNoth\MarkupCheck\Tests\Html\DocumentTest::testGetHeadings()
     * @since 1.0.0
     */
    public function getHeadingsParamProvider(): array
    {
        return [
            ['', [], true],
            ['some text', [], true],
            [null, [], true],
            [INVALID_MARKUP, [], true],
            [
                VALID_MARKUP,
                [
                    ['name' => 'h1', 'rank' => 1, 'value' => 'H1 Headline', 'attributes' => []],
                    ['name' => 'h2', 'rank' => 2, 'value' => '<span>H2 Headline</span>', 'attributes' => []],
                ],
                false,
            ],
            [
                strtoupper(VALID_MARKUP),
                [
                    ['name' => 'h1', 'rank' => 1, 'value' => 'H1 HEADLINE', 'attributes' => []],
                    ['name' => 'h2', 'rank' => 2, 'value' => '<SPAN>H2 HEADLINE</SPAN>', 'attributes' => []],
                ],
                false,
            ],
            [VALID_URL, [], true],
            [INVALID_URL, [], true],
        ];
    }

    /**
     * Provides data for testing HtmlDocument::getAnchors().
     *
     * @return array[]
     * @see \OliverNoth\MarkupCheck\Tests\Html\DocumentTest::testGetAnchors()
     * @since 1.0.0
     */
    public function getAnchorsParamProvider(): array
    {
        return [
            ['', [], true],
            ['some text', [], true],
            [null, [], true],
            [INVALID_MARKUP, [], true],
            [
                VALID_MARKUP,
                [
                    [
                        'name' => 'a',
                        'value' => '<img src="//www.example.com/user.png" alt="User image">',
                        'attributes' => ['href' => 'mailto:user@example.com'],
                    ],
                ],
                false,
            ],
            [VALID_URL, [], true],
            [INVALID_URL, [], true],
        ];
    }

    /**
     * Provides data for testing HtmlDocument::getImages().
     *
     * @return array[]
     * @see \OliverNoth\MarkupCheck\Tests\Html\DocumentTest::testGetImages()
     * @since 1.0.0
     */
    public function getImagesParamProvider(): array
    {
        return [
            ['', [], true],
            ['some text', [], true],
            [null, [], true],
            [INVALID_MARKUP, [], true],
            [
                VALID_MARKUP,
                [['name' => 'img', 'attributes' => ['src' => '//www.example.com/user.png', 'alt' => 'User image']]],
                false,
            ],
            [VALID_URL, [], true],
            [INVALID_URL, [], true],
        ];
    }

    /**
     * Tests, if a new HtmlDocument-Instance can be created from valid constructor params.
     *
     * @dataProvider constructorParamProvider
     * @covers ::__construct
     * @covers ::init
     * @param string|null $param
     * @param bool $expectsException
     * @return void
     * @since 1.0.0
     */
    public function testCanBeCreatedFromConstructorparams(?string $param, bool $expectsException): void
    {
        if ($expectsException) {
            $this->expectException('InvalidArgumentException');
        }

        $this->assertInstanceOf(HtmlDocument::class, new HtmlDocument($param));
    }

    /**
     * Tests, if each setter method of a new HtmlDocument-Instance returns the instance itself.
     *
     * @covers ::__construct
     * @covers ::init
     * @covers ::setMarkup
     * @covers ::setDom
     * @return void
     * @since 1.0.0
     */
    public function testSetterMethodsAreChainable(): void
    {
        $htmlDocument = new HtmlDocument(VALID_MARKUP);

        $this->assertInstanceOf(HtmlDocument::class, $htmlDocument->setMarkup(VALID_MARKUP));
        $this->assertInstanceOf(HtmlDocument::class, $htmlDocument->setDom(new \DOMDocument(VALID_MARKUP)));
    }

    /**
     * Tests HtmlDocument getter methods.
     *
     * @covers ::__construct
     * @covers ::init
     * @covers ::getMarkup
     * @covers ::getDom
     * @return void
     * @since 1.0.0
     */
    public function testGetterMethods(): void
    {
        $htmlDocument = new HtmlDocument(VALID_MARKUP);

        $domDocument = new \DOMDocument(VALID_MARKUP);

        $htmlDocument
            ->setMarkup(VALID_MARKUP)
            ->setDom($domDocument)
        ;

        $this->assertSame(VALID_MARKUP, $htmlDocument->getMarkup());
        $this->assertSame($domDocument, $htmlDocument->getDom());
    }

    /**
     * Tests HtmlDocument::containsHtmlMarkup().
     *
     * @dataProvider containsHtmlMarkupProvider
     * @param string|null $param
     * @param bool $expected
     * @param bool $expectsException
     * @covers ::__construct
     * @covers ::init
     * @covers ::containsHtmlMarkup
     * @return void
     * @throws \ReflectionException
     * @since 1.0.0
     */
    public function testContainsHtmlMarkup(?string $param, bool $expected, bool $expectsException): void
    {
        if ($expectsException) {
            $this->expectException('InvalidArgumentException');
        }

        $htmlDocument = new HtmlDocument($param);

        $reflection = new \ReflectionClass($htmlDocument);
        $method = $reflection->getMethod('containsHtmlMarkup');
        $method->setAccessible(true);

        $this->assertSame($expected, $method->invoke($htmlDocument, $param));
    }

    /**
     * Tests HtmlDocument::getDOMDocument().
     *
     * @covers ::__construct
     * @covers ::init
     * @covers ::getDOMDocument
     * @return void
     * @throws \ReflectionException
     * @since 1.0.0
     */
    public function testGetDOMDocument(): void
    {
        $htmlDocument = new HtmlDocument(VALID_MARKUP);

        $reflection = new \ReflectionClass($htmlDocument);
        $method = $reflection->getMethod('getDOMDocument');
        $method->setAccessible(true);

        $this->assertInstanceOf(\DOMDocument::class, $method->invoke($htmlDocument, VALID_MARKUP));
    }

    /**
     * Tests HtmlDocument::getDOMDocument().
     *
     * @dataProvider getDOMDocumentThrowsExceptionParamProvider
     * @param string|null $param
     * @param string $exceptionType
     * @param string $exceptionMessage
     * @covers ::__construct
     * @covers ::init
     * @covers ::getDOMDocument
     * @return void
     * @throws \ReflectionException
     * @since 1.0.0
     */
    public function testGetDOMDocumentThrowsException(?string $param, string $exceptionType, string $exceptionMessage): void
    {
        $this->expectException($exceptionType);
        $this->expectExceptionMessage($exceptionMessage);

        $htmlDocument = new HtmlDocument($param);

        $reflection = new \ReflectionClass($htmlDocument);
        $method = $reflection->getMethod('getDOMDocument');
        $method->setAccessible(true);

        $method->invoke($htmlDocument, $param);
    }

    /**
     * Tests HtmlDocument::getDOMElementAsArray().
     *
     * @dataProvider getDOMElementAsArrayParamProvider
     * @param \DOMElement $element
     * @param array $expected
     * @covers ::__construct
     * @covers ::init
     * @covers ::getDOMElementAsArray
     * @return void
     * @throws \ReflectionException
     * @since 1.0.0
     */
    public function testGetDOMElementAsArray(\DOMElement $element, array $expected): void
    {
        $htmlDocument = new HtmlDocument(VALID_MARKUP);

        $reflection = new \ReflectionClass($htmlDocument);
        $method = $reflection->getMethod('getDOMElementAsArray');
        $method->setAccessible(true);

        $this->assertSame($expected, $method->invoke($htmlDocument, $element));
    }

    /**
     * Tests HtmlDocument::getDOMElementAsArray().
     *
     * @dataProvider getDOMElementAsArrayErrorOccursParamProvider
     * @param string|null $element
     * @covers ::__construct
     * @covers ::init
     * @covers ::getDOMElementAsArray
     * @return void
     * @throws \ReflectionException
     * @since 1.0.0
     */
    public function testGetDOMElementAsArrayErrorOccurs(?string $element): void
    {
        if (is_null($element)) {
            $this->expectError();
        } else {
            $this->expectException('TypeError');
        }

        $htmlDocument = new HtmlDocument(VALID_MARKUP);

        $reflection = new \ReflectionClass($htmlDocument);
        $method = $reflection->getMethod('getDOMElementAsArray');
        $method->setAccessible(true);

        $method->invoke($htmlDocument, $element);
    }

    /**
     * Tests HtmlDocument::getElementsByTagName().
     *
     * @dataProvider getElementsByTagNameParamProvider
     * @param string|null $tag
     * @param array $expected
     * @covers ::__construct
     * @covers ::init
     * @covers ::getElementsByTagName
     * @covers ::getDOMElementAsArray
     * @return void
     * @since 1.0.0
     */
    public function testGetElementsByTagName(?string $tag, array $expected): void
    {
        $htmlDocument = new HtmlDocument(VALID_MARKUP);

        $this->assertSame($expected, $htmlDocument->getElementsByTagName($tag));
    }

    /**
     * Tests HtmlDocument::getHeadings().
     *
     * @dataProvider getHeadingsParamProvider
     * @param string|null $param
     * @param array $expected
     * @param bool $expectsException
     * @covers ::__construct
     * @covers ::init
     * @covers ::getHeadings
     * @return void
     * @since 1.0.0
     */
    public function testGetHeadings(?string $param, array $expected, bool $expectsException): void
    {
        if ($expectsException) {
            $this->expectException('InvalidArgumentException');
        }

        $htmlDocument = new HtmlDocument($param);

        $this->assertSame($expected, $htmlDocument->getHeadings());
    }

    /**
     * Tests HtmlDocument::getAnchors().
     *
     * @dataProvider getAnchorsParamProvider
     * @param string|null $param
     * @param array $expected
     * @param bool $expectsException
     * @covers ::__construct
     * @covers ::init
     * @covers ::getAnchors
     * @return void
     * @since 1.0.0
     */
    public function testGetAnchors(?string $param, array $expected, bool $expectsException): void
    {
        if ($expectsException) {
            $this->expectException('InvalidArgumentException');
        }

        $htmlDocument = new HtmlDocument($param);

        $this->assertSame($expected, $htmlDocument->getAnchors());
    }

    /**
     * Tests HtmlDocument::getImages().
     *
     * @dataProvider getImagesParamProvider
     * @param string|null $param
     * @param array $expected
     * @param bool $expectsException
     * @covers ::__construct
     * @covers ::init
     * @covers ::getImages
     * @return void
     * @since 1.0.0
     */
    public function testGetImages(?string $param, array $expected, bool $expectsException): void
    {
        if ($expectsException) {
            $this->expectException('InvalidArgumentException');
        }

        $htmlDocument = new HtmlDocument($param);

        $this->assertSame($expected, $htmlDocument->getImages());
    }
}