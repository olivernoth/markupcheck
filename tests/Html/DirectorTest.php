<?php
declare(strict_types=1);
/**
 * Test case for OliverNoth\MarkupCheck\Html\Director
 *   - Contains methods for testing the behaviour of the tested class
 *
 * @class DirectorTest
 * @author Oliver Noth <info@nothbetrieb.de>
 * @copyright 2020 Oliver Noth
 */

namespace OliverNoth\MarkupCheck\Tests\Html;

use \PHPUnit\Framework\TestCase;
use \OliverNoth\MarkupCheck\{Html\Director as HtmlDirector, Html\Document as HtmlDocument, Helper\Url};

/**
 * Class DirectorTest
 *
 * @coversDefaultClass \OliverNoth\MarkupCheck\Html\Director
 * @group director
 * @package OliverNoth\MarkupCheck\Tests\Html
 * @since 1.0.0
 */
final class DirectorTest extends TestCase
{
    /**
     * Provides data for testing HtmlDirector::__construct().
     *
     * @return array[]
     * @see \OliverNoth\MarkupCheck\Tests\Html\DirectorTest::testCanBeCreatedFromConstructorparams()
     * @since 1.0.0
     */
    public function constructorParamProvider(): array
    {
        return [
            [VALID_URL],
            [INVALID_URL],
            [VALID_MARKUP],
            [INVALID_MARKUP],
            [''],
        ];
    }

    /**
     * Provides data for testing, whether a new HtmlDirector-Instance sets its context correctly.
     *
     * @return array[]
     * @see \OliverNoth\MarkupCheck\Tests\Html\DirectorTest::testContextIsSetCorrectlyAfterCreation()
     * @since 1.0.0
     */
    public function contextParamProvider(): array
    {
        return [
            ['', ['CONTEXT_ERROR', 'CONTEXT_NEITHER_URL_NOR_MARKUP_GIVEN']],
            [null, ['CONTEXT_ERROR', 'CONTEXT_NEITHER_URL_NOR_MARKUP_GIVEN']],
            [VALID_URL, ['CONTEXT_NO_ERROR', 'CONTEXT_URL_GIVEN']],
            [INVALID_URL, ['CONTEXT_ERROR', 'CONTEXT_URL_GIVEN']],
            [VALID_MARKUP, ['CONTEXT_NO_ERROR', 'CONTEXT_MARKUP_GIVEN']],
            [INVALID_MARKUP, ['CONTEXT_ERROR', 'CONTEXT_NEITHER_URL_NOR_MARKUP_GIVEN']],
        ];
    }

    /**
     * Provides data for testing HtmlDirector::getHeadingNotes().
     *
     * @return array
     * @see \OliverNoth\MarkupCheck\Tests\Html\DirectorTest::testGetHeadingNotes()
     * @since 1.0.0
     */
    public function getHeadingNotesParamProvider(): array
    {
        $reflection = new \ReflectionClass(HtmlDirector::class);

        return [
            [[], []],
            [null, []],
            [
                [['name' => 'h1', 'rank' => '1', 'value' => 'Foo']],
                [$reflection->getConstant('EXACTLY_ONE_H1_TAG'), $reflection->getConstant('H_TAGS_IN_RIGHT_ORDER')],
            ],
            [
                [['name' => 'h1', 'rank' => 1, 'value' => 'Foo']],
                [$reflection->getConstant('EXACTLY_ONE_H1_TAG'), $reflection->getConstant('H_TAGS_IN_RIGHT_ORDER')],
            ],
            [
                [['name' => 'h1', 'rank' => 1, 'value' => 'Foo'], ['name' => 'h1', 'rank' => 1, 'value' => 'Bar']],
                [
                    sprintf($reflection->getConstant('MORE_THAN_ONE_H1_TAG'), 2),
                    $reflection->getConstant('H_TAGS_IN_RIGHT_ORDER'),
                ],
            ],
            [
                [['name' => 'h2', 'rank' => '2', 'value' => 'Foo']],
                [$reflection->getConstant('NO_H1_TAG'), $reflection->getConstant('H_TAGS_IN_RIGHT_ORDER')],
            ],
            [
                [['name' => 'h2', 'rank' => 2, 'value' => 'Foo']],
                [$reflection->getConstant('NO_H1_TAG'), $reflection->getConstant('H_TAGS_IN_RIGHT_ORDER')],
            ],
            [
                [['name' => 'h2', 'rank' => 2, 'value' => 'Foo'], ['name' => 'h1', 'rank' => 1, 'value' => 'Bar']],
                [
                    sprintf($reflection->getConstant('EXACTLY_ONE_H1_TAG'), 2),
                    $reflection->getConstant('H_TAGS_NOT_IN_RIGHT_ORDER'),
                ],
            ],
            [
                [['name' => 'h3', 'rank' => 3, 'value' => 'Foo'], ['name' => 'h2', 'rank' => 2, 'value' => 'Bar']],
                [
                    sprintf($reflection->getConstant('NO_H1_TAG'), 2),
                    $reflection->getConstant('H_TAGS_NOT_IN_RIGHT_ORDER'),
                ],
            ],
        ];
    }

    /**
     * Provides data for testing HtmlDirector::getAnchorNotes().
     *
     * @return array[]
     * @see \OliverNoth\MarkupCheck\Tests\Html\DirectorTest::testGetAnchorNotes()
     * @since 1.0.0
     */
    public function getAnchorNotesParamProvider(): array
    {
        $reflection = new \ReflectionClass(HtmlDirector::class);

        return [
            [[], []],
            [null, []],
            [
                [
                    [
                        'name' => 'a',
                        'value' => 'Foo',
                        'attributes' => ['href' => preg_replace('/http[s]*:\//', '', VALID_URL)],
                    ],
                ],
                [$reflection->getConstant('ALL_A_TAGS_LINKED_CORRECTLY')],
            ],
            [
                [['name' => 'a', 'value' => 'Foo', 'attributes' => ['href' => VALID_URL]]],
                [$reflection->getConstant('MORE_THAN_ONE_INTERNAL_ABSOLUTE_A_TAG')],
            ],
        ];
    }

    /**
     * Provides data for testing HtmlDirector::getImageNotes().
     *
     * @return array[]
     * @see \OliverNoth\MarkupCheck\Tests\Html\DirectorTest::testGetImageNotes()
     * @since 1.0.0
     */
    public function getImageNotesParamProvider(): array
    {
        $reflection = new \ReflectionClass(HtmlDirector::class);

        return [
            [[], []],
            [null, []],
            [
                [['name' => 'img', 'attributes' => ['alt' => 'Foo']]],
                [$reflection->getConstant('ALL_IMG_TAGS_COMPLETE')],
            ],
            [
                [['name' => 'img', 'attributes' => ['title' => 'Foo']]],
                [$reflection->getConstant('ALL_IMG_TAGS_COMPLETE')],
            ],
            [[['name' => 'img', 'attributes' => []]], [$reflection->getConstant('ONE_OR_MORE_IMG_TAG_NOT_COMPLETE')]],
        ];
    }

    /**
     * Provides data for testing HtmlDirector::getParsedTags().
     *
     * @return array[]
     * @see \OliverNoth\MarkupCheck\Tests\Html\DirectorTest::testGetParsedTags()
     * @since 1.0.0
     */
    public function getParsedTagsParamProvider(): array
    {
        $reflection = new \ReflectionClass(HtmlDirector::class);

        return [
            [
                INVALID_MARKUP,
                [
                    'errors' => [
                        'notes' => [
                            'Invalid markup provided.',
                            'Neither a valid url nor a valid markup has been provided.',
                        ],
                        'tags' => [],
                    ],
                ],
            ],
            [
                VALID_MARKUP,
                [
                    'headings' => [
                        'notes' => [
                            $reflection->getConstant('EXACTLY_ONE_H1_TAG'),
                            $reflection->getConstant('H_TAGS_IN_RIGHT_ORDER'),
                        ],
                        'tags' => [
                            ['name' => 'h1', 'rank' => 1, 'value' => 'H1 Headline', 'attributes' => []],
                            ['name' => 'h2', 'rank' => 2, 'value' => '<span>H2 Headline</span>', 'attributes' => []],
                        ],
                    ],
                    'anchors' => [
                        'notes' => [$reflection->getConstant('ALL_A_TAGS_LINKED_CORRECTLY')],
                        'tags' => [
                            [
                                'name' => 'a',
                                'value' => '<img src="//www.example.com/user.png" alt="User image">',
                                'attributes' => ['href' => 'mailto:user@example.com'],
                            ],
                        ],
                    ],
                    'images' => [
                        'notes' => [$reflection->getConstant('ALL_IMG_TAGS_COMPLETE')],
                        'tags' => [
                            [
                                'name' => 'img',
                                'attributes' => ['src' => '//www.example.com/user.png', 'alt' => 'User image'],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Provides data for testing HtmlDirector::renderParsedTagsAsJson().
     *
     * @return array[]
     * @see \OliverNoth\MarkupCheck\Tests\Html\DirectorTest::testRenderParsedTagsAsJson()
     * @since 1.0.0
     */
    public function renderParsedTagsAsJsonParamProvider(): array
    {
        return array_map(
            function(array $provided) {
                $provided[1] = json_encode($provided[1]);

                return $provided;
            },
            $this->getParsedTagsParamProvider()
        );
    }

    /**
     * Provides data for testing HtmlDirector::getLabel().
     *
     * @return array[]
     * @see \OliverNoth\MarkupCheck\Tests\Html\DirectorTest::testGetLabel()
     * @since 1.0.0
     */
    public function getLabelParamProvider(): array
    {
        return [
            ['', '(errors occurred)'],
            [null, '(errors occurred)'],
            [VALID_URL, sprintf('Given uri "%s"', VALID_URL)],
            [INVALID_URL, sprintf('Given uri "%s" (errors occurred)', INVALID_URL)],
            [
                VALID_MARKUP,
                'Given markup "<h1>H1 Headline</h1><h2><span>H2 Headline</span></h2><a href="mailto:user@exa..."',
            ],
            [INVALID_MARKUP, INVALID_MARKUP . ' (errors occurred)'],
        ];
    }

    /**
     * Tests, if a new HtmlDirector-Instance can be created from valid constructor params.
     *
     * @dataProvider constructorParamProvider
     * @covers ::__construct
     * @covers ::init
     * @param string $maybeUri
     * @return void
     * @since 1.0.0
     */
    public function testCanBeCreatedFromConstructorparams(string $maybeUri): void
    {
        $this->assertInstanceOf(HtmlDirector::class, new HtmlDirector($maybeUri));
    }

    /**
     * Tests, if each setter method of a new HtmlDirector-Instance returns the instance itself.
     *
     * @covers ::__construct
     * @covers ::init
     * @covers ::setUrl
     * @covers ::setHtmlDocument
     * @return void
     * @since 1.0.0
     */
    public function testSetterMethodsAreChainable(): void
    {
        $director = new HtmlDirector();

        $this->assertInstanceOf(HtmlDirector::class, $director->setUrl(new Url(VALID_URL)));
        $this->assertInstanceOf(HtmlDirector::class, $director->setHtmlDocument(new HtmlDocument(VALID_MARKUP)));
    }

    /**
     * Tests, if a new HtmlDirector-Instance sets its context correctly.
     *
     * @dataProvider contextParamProvider
     * @covers ::__construct
     * @covers ::init
     * @param string|null $param
     * @param string[] $constants
     * @return void
     * @throws \ReflectionException
     * @since 1.0.0
     */
    public function testContextIsSetCorrectlyAfterCreation(?string $param, array $constants): void
    {
        if (is_null($param)) {
            $this->expectException('TypeError');
        }

        $director = new HtmlDirector($param);

        $reflection = new \ReflectionClass($director);
        $contextProperty = $reflection->getProperty('context');
        $contextProperty->setAccessible(true);

        foreach ($constants as $constant) {
            $constant = $reflection->getConstant($constant);
            $this->assertTrue($constant === ($constant & $contextProperty->getValue($director)));
        }
    }

    /**
     * Tests HtmlDirector::getHeadingNotes().
     *
     * @dataProvider getHeadingNotesParamProvider
     * @covers ::__construct
     * @covers ::init
     * @covers ::getHeadingNotes
     * @param array|null $hTagElements
     * @param array|null $expected
     * @return void
     * @throws \ReflectionException
     * @since 1.0.0
     */
    public function testGetHeadingNotes(?array $hTagElements, ?array $expected): void
    {
        $director = new HtmlDirector(VALID_URL);

        $reflection = new \ReflectionClass($director);
        $method = $reflection->getMethod('getHeadingNotes');
        $method->setAccessible(true);

        $this->assertSame($expected, $method->invoke($director, $hTagElements));
    }

    /**
     * Tests HtmlDirector::getAnchorNotes().
     *
     * @dataProvider getAnchorNotesParamProvider
     * @covers ::__construct
     * @covers ::init
     * @covers ::getAnchorNotes
     * @param array|null $aTagElements
     * @param array|null $expected
     * @return void
     * @throws \ReflectionException
     * @since 1.0.0
     */
    public function testGetAnchorNotes(?array $aTagElements, ?array $expected): void
    {
        $director = new HtmlDirector(VALID_URL);

        $reflection = new \ReflectionClass($director);
        $method = $reflection->getMethod('getAnchorNotes');
        $method->setAccessible(true);

        $this->assertSame($expected, $method->invoke($director, $aTagElements));
    }

    /**
     * Tests HtmlDirector::getImageNotes().
     *
     * @dataProvider getImageNotesParamProvider
     * @covers ::__construct
     * @covers ::init
     * @covers ::getImageNotes
     * @param array|null $imgTagElements
     * @param array|null $expected
     * @return void
     * @throws \ReflectionException
     * @since 1.0.0
     */
    public function testGetImageNotes(?array $imgTagElements, ?array $expected): void
    {
        $director = new HtmlDirector(VALID_URL);

        $reflection = new \ReflectionClass($director);
        $method = $reflection->getMethod('getImageNotes');
        $method->setAccessible(true);

        $this->assertSame($expected, $method->invoke($director, $imgTagElements));
    }

    /**
     * Tests HtmlDirector::getParsedTags().
     *
     * @dataProvider getParsedTagsParamProvider
     * @covers ::__construct
     * @covers ::init
     * @covers ::getParsedTags
     * @param string $param
     * @param array $expected
     * @return void
     * @since 1.0.0
     */
    public function testGetParsedTags(string $param, array $expected): void
    {
        $director = new HtmlDirector($param);

        $this->assertSame($expected, $director->getParsedTags());
    }

    /**
     * Tests HtmlDirector::renderParsedTagsAsJson().
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @dataProvider renderParsedTagsAsJsonParamProvider
     * @covers ::__construct
     * @covers ::init
     * @covers ::getParsedTags
     * @covers ::renderParsedTagsAsJson
     * @param string $param
     * @param string $expected
     * @return void
     * @since 1.0.0
     */
    public function testRenderParsedTagsAsJson(string $param, string $expected): void
    {
        ob_start();

        (new HtmlDirector($param))->renderParsedTagsAsJson();

        $json = ob_get_clean();

        $this->assertSame($expected, $json);
    }

    /**
     * Tests HtmlDirector::getLabel().
     *
     * @dataProvider getLabelParamProvider
     * @covers ::__construct
     * @covers ::init
     * @covers ::getLabel
     * @param string|null $param
     * @param string $expected
     * @return void
     * @since 1.0.0
     */
    public function testGetLabel(?string $param, string $expected): void
    {
        if (is_null($param)) {
            $this->expectException('TypeError');
        }

        $director = new HtmlDirector($param);

        $this->assertSame($expected, $director->getLabel());
    }
}