<?php
declare(strict_types=1);
/**
 * Test case for OliverNoth\MarkupCheck\Helper\Url
 *   - Contains methods for testing the behaviour of the tested class
 *
 * @class UrlTest
 * @author Oliver Noth <info@nothbetrieb.de>
 * @copyright 2020 Oliver Noth
 */

namespace OliverNoth\MarkupCheck\Tests\Helper;

use \PHPUnit\Framework\TestCase;
use \OliverNoth\MarkupCheck\Helper\Url;

/**
 * Class UrlTest
 *
 * @coversDefaultClass \OliverNoth\MarkupCheck\Helper\Url
 * @group url
 * @package OliverNoth\MarkupCheck\Tests\Helper
 * @since 1.0.0
 */
final class UrlTest extends TestCase
{
    /**
     * Provides data for testing Url::__construct().
     *
     * @return array[]
     * @see \OliverNoth\MarkupCheck\Tests\Helper\UrlTest::testCanBeCreatedFromConstructorparams()
     * @since 1.0.0
     */
    public function constructorParamProvider(): array
    {
        return [
            [''],
            [null],
            ['https://user:pass@www.example.com/'],
            ['https://www.example.foo.bar/'],
        ];
    }

    /**
     * Provides data for Url constructor sets its instance properties.
     *
     * @return array[]
     * @see \OliverNoth\MarkupCheck\Tests\Helper\UrlTest::testProperties()
     * @see \OliverNoth\MarkupCheck\Tests\Helper\UrlTest::testMagicalGet()
     * @since 1.0.0
     */
    public function propertiesParamProvider(): array
    {
        return [
            [
                '',
                [
                    'uri' => '',
                    'scheme' => '',
                    'host' => '',
                    'port' => '',
                    'path' => '',
                    'user' => '',
                    'pass' => '',
                    'query' => '',
                ],
            ],
            [
                null,
                [
                    'uri' => '',
                    'scheme' => '',
                    'host' => '',
                    'port' => '',
                    'path' => '',
                    'user' => '',
                    'pass' => '',
                    'query' => '',
                ],
            ],
            [
                'https://user:pass@www.example.com/',
                [
                    'uri' => 'https://user:pass@www.example.com/',
                    'scheme' => 'https',
                    'host' => 'www.example.com',
                    'port' => '',
                    'path' => '/',
                    'user' => 'user',
                    'pass' => 'pass',
                    'query' => '',
                ],
            ],
            [
                'http://user:pass@www.example.com/',
                [
                    'uri' => 'http://user:pass@www.example.com/',
                    'scheme' => 'http',
                    'host' => 'www.example.com',
                    'port' => '',
                    'path' => '/',
                    'user' => 'user',
                    'pass' => 'pass',
                    'query' => '',
                ],
            ],
            [
                'www.example.com',
                [
                    'uri' => 'https://www.example.com/',
                    'scheme' => 'https',
                    'host' => 'www.example.com',
                    'port' => '',
                    'path' => '/',
                    'user' => '',
                    'pass' => '',
                    'query' => '',
                ],
            ],
            [
                'www.example.com/user/?id=1',
                [
                    'uri' => 'https://www.example.com/user/?id=1',
                    'scheme' => 'https',
                    'host' => 'www.example.com',
                    'port' => '',
                    'path' => '/user',
                    'user' => '',
                    'pass' => '',
                    'query' => '/?id=1',
                ],
            ],
        ];
    }

    /**
     * Provides data for Url::__toString().
     *
     * @return array[]
     * @see \OliverNoth\MarkupCheck\Tests\Helper\UrlTest::testToString()
     * @since 1.0.0
     */
    public function toStringParamProvider(): array
    {
        return [
            ['', ''],
            [null, ''],
            [VALID_URL, rtrim(VALID_URL, '/') . '/'],
            [INVALID_URL, rtrim(INVALID_URL, '/') . '/'],
            [VALID_MARKUP, ''],
            [INVALID_MARKUP, ''],
            ['http://localhost:8000', 'http://localhost:8000/'],
            ['https://user:pass@www.example.com/', 'https://user:pass@www.example.com/'],
            ['http://user:pass@www.example.com/', 'http://user:pass@www.example.com/'],
            ['www.example.com', 'https://www.example.com/'],
            ['www.example.com/user/?id=1', 'https://www.example.com/user/?id=1'],
        ];
    }

    /**
     * Provides data for Url::determineMarkup().
     *
     * @return array[]
     * @see \OliverNoth\MarkupCheck\Tests\Helper\UrlTest::testDetermineMarkup()
     * @since 1.0.0
     */
    public function determineMarkupParamProvider(): array
    {
        return [
            [VALID_URL, VALID_MARKUP, false],
            ['', '', true],
            [null, '', true],
            [INVALID_URL, '', true],
            [VALID_MARKUP, '', true],
            [INVALID_MARKUP, '', true],
        ];
    }

    /**
     * Tests Url::__construct().
     *
     * @dataProvider constructorParamProvider
     * @covers ::__construct
     * @covers ::init
     * @param string|null $param
     * @return void
     * @since 1.0.0
     */
    public function testCanBeCreatedFromConstructorparams(?string $param): void
    {
        $this->assertInstanceOf(Url::class, new Url($param));
    }

    /**
     * Tests Url constructor sets its instance properties.
     *
     * @dataProvider propertiesParamProvider
     * @covers ::__construct
     * @covers ::init
     * @param string|null $param
     * @param array $expected
     * @return void
     * @throws \ReflectionException
     * @since 1.0.0
     */
    public function testProperties(?string $param, array $expected): void
    {
        $url = new Url($param);

        $reflection = new \ReflectionClass($url);
        $properties = $reflection->getProperty('properties');
        $properties->setAccessible(true);

        $this->assertSame($expected, $properties->getValue($url));
    }

    /**
     * Tests Url::__get().
     *
     * @dataProvider propertiesParamProvider
     * @covers ::__construct
     * @covers ::init
     * @covers ::__get
     * @param string|null $param
     * @param array $properties
     * @return void
     * @since 1.0.0
     */
    public function testMagicalGet(?string $param, array $properties): void
    {
        $url = new Url($param);

        foreach ($properties as $property => $value) {
            $this->assertSame($value, $url->{$property});
        }
    }

    /**
     * Tests Url::__toString().
     *
     * @dataProvider toStringParamProvider
     * @covers ::__construct
     * @covers ::init
     * @covers ::__toString
     * @param string|null $param
     * @param string $expected
     * @return void
     * @since 1.0.0
     */
    public function testToString(?string $param, string $expected): void
    {
        $url = new Url($param);

        $this->assertSame($expected, (string)$url);
    }

    /**
     * Tests Url::__determineMarkup().
     *
     * @dataProvider determineMarkupParamProvider
     * @covers ::__construct
     * @covers ::init
     * @covers ::determineMarkup
     * @param string|null $param
     * @param string $expected
     * @param bool $expectsException
     * @return void
     * @throws \Exception
     * @since 1.0.0
     */
    public function testDetermineMarkup(?string $param, string $expected, bool $expectsException): void
    {
        if ($expectsException) {
            $this->expectException('Exception');
        }

        $url = new Url($param);

        $this->assertSame($expected, $url->determineMarkup());
    }
}