<?php
declare(strict_types=1);
/**
 * Test case for OliverNoth\MarkupCheck\Factory
 *   - Contains methods for testing the behaviour of the tested class
 *
 * @class FactoryTest
 * @author Oliver Noth <info@nothbetrieb.de>
 * @copyright 2020 Oliver Noth
 */

namespace OliverNoth\MarkupCheck\Tests\Main;

use \PHPUnit\Framework\TestCase;
use OliverNoth\MarkupCheck\Main\Factory;
use OliverNoth\MarkupCheck\Html\Director as HtmlDirector;

/**
 * Class FactoryTest
 *
 * @coversDefaultClass \OliverNoth\MarkupCheck\Main\Factory
 * @group factory
 * @package OliverNoth\MarkupCheck\Tests
 * @since 1.0.0
 */
final class FactoryTest extends TestCase
{
    /**
     * Provides data for testing Factory::createHtmlMarkupChecker.
     *
     * @return array[]
     * @see \OliverNoth\MarkupCheck\Tests\Main\FactoryTest::testCreateHtmlMarkupChecker()
     * @since 1.0.0
     */
    public function createHtmlMarkupCheckerProvider(): array
    {
        return [
            ['', HtmlDirector::class],
            [null, HtmlDirector::class],
            [VALID_URL, HtmlDirector::class],
            [INVALID_URL, HtmlDirector::class],
            [VALID_MARKUP, HtmlDirector::class],
            [INVALID_MARKUP, HtmlDirector::class],
        ];
    }

    /**
     * Tests, if a new Factory-Instance can create a new HtmlMarkupChecker.
     *
     * @dataProvider createHtmlMarkupCheckerProvider
     * @covers ::createHtmlMarkupChecker
     * @param string|null $param
     * @param string $expected
     * @return void
     * @since 1.0.0
     */
    public function testCreateHtmlMarkupChecker(?string $param, string $expected): void
    {
        if (is_null($param)) {
            $this->expectException('TypeError');
        }

        $this->assertInstanceOf($expected, (new Factory())->createHtmlMarkupChecker($param));
    }
}