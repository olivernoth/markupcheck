<?php declare(strict_types=1);
/*
 * This file is part of markupcheck/tests.
 * It acts as phpunit bootstrap file.
 *
 * (c) Oliver Noth <info@nothbetrieb.de>
 */

// Constants used in test cases
\define('DS', DIRECTORY_SEPARATOR);
\defined('WEB_SERVER_HOST') || \define('WEB_SERVER_HOST', 'localhost');
\defined('WEB_SERVER_PORT') || \define('WEB_SERVER_PORT', 9999);
\defined('WEB_SERVER_DOCROOT') || \define('WEB_SERVER_DOCROOT', 'tests' . DS . '_files' . DS . 'webserver_docroot tests' . DS . '_files' . DS . 'webserver_docroot' . DS . 'index.php');
\define('VALID_URL', sprintf('http://%s:%s/', WEB_SERVER_HOST, WEB_SERVER_PORT));
\define('INVALID_URL', sprintf('https://%s:%s/not_exists.php', WEB_SERVER_HOST, WEB_SERVER_PORT));
\define('VALID_MARKUP', '<h1>H1 Headline</h1><h2><span>H2 Headline</span></h2><a href="mailto:user@example.com"><img src="//www.example.com/user.png" alt="User image"></a>');
\define('INVALID_MARKUP', '<span><invalidMarkup</span>');

// Start php built-in web server and store the process ID
$output = [];
\exec(sprintf('VALID_MARKUP=%s php -d variables_order=EGPCS -S %s:%d -t %s >/dev/null 2>&1 & echo $!', base64_encode(VALID_MARKUP), WEB_SERVER_HOST, WEB_SERVER_PORT, WEB_SERVER_DOCROOT), $output);
$pid = (int) $output[0];

// Kill the web server when the process ends
\register_shutdown_function(function() use ($pid) {

  if (false !== ($output = []) && exec('ps ' . $pid, $output) && is_array($output) && 2 <= count($output)) {
    exec('kill -KILL ' . $pid);
  }
});

// Class loader
require_once __DIR__ . '/../vendor/autoload.php';