# MarkupCheck

MarkupCheck is a [composer](https://getcomposer.org/) package, which parses and checks certain tags in an html markup given by either an url to fetch the markup from or the markup itself.

[![Latest Stable Version](https://img.shields.io/static/v1?label=packagist&message=1.2.4&color=informational&style=flat-square)](https://packagist.org/packages/oliver-noth/markup-check#v1.2.4)
[![Minimum PHP Version](https://img.shields.io/static/v1?label=php&message=%3E%3D8.0&color=8892BF&style=flat-square)](https://php.net/)
[![Type Coverage](https://img.shields.io/static/v1?label=coverage&message=100%&color=green&style=flat-square)](https://phpunit.readthedocs.io/en/9.1/code-coverage-analysis.html)
[![Semantic Versioning](https://img.shields.io/static/v1?label=semver&message=1.2.4&color=yellowgreen&style=flat-square)](http://semver.org/)
[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](LICENSE)

## Table of contents

1. [Getting started](#getting-started)
    1. [Requirements](#requirements)
    1. [Installation](#installation)
1. [Usage](#usage)
    1. [Basic](#basic)
    1. [Further](#further)
1. [Docker](#docker)
    1. [Prerequisites](#prerequisites)
    1. [Setup](#setup)
    1. [Create and run docker container](#create-a-new-docker-container)
    1. [Unit tests](#unit-tests)
    1. [Digging deeper](#digging-deeper)
1. [Authors](#authors)

## Getting Started

#### Requirements

* Your local machine must run with at least PHP 8.0
* Be sure to have [composer](https://getcomposer.org/) installed

#### Installation

Use composer to download and install MarkupCheck.

You can add MarkupCheck as a local, per-project dependency to your project:

```bash
$ composer require oliver-noth/markupcheck
```

If you only need MarkupCheck during development, then you should add it as a development-time dependency:

```bash
$ composer require --dev oliver-noth/markupcheck
```

## Usage

#### Basic

First instantiate a new markup checker

```php
$factory = new \OliverNoth\MarkupCheck\Main\Factory();
$markupChecker = $factory->createHtmlMarkupChecker('https://getcomposer.org/');
```

then get the parsed tags

```php
$parsedTags = $markupChecker->getParsedTags();
```

or render them as json

```php
$markupChecker->renderParsedTagsAsJson();
exit;
```

Parsed tags will be provided in an array (example shown below) 

```php
[
    'headings' => [
        'notes' => [
            'Markup contains exactly 1 first heading (h1 tag).',
        ],
        'tags' => [
            0 => [
                'name'  => 'h1',
                'rank'  => 1,
                'value' => 'First headline',
                'attributes' => [
                    'class'  => 'article',
                ],
            ],
        ],
    ],
    'anchors' => [
        'notes' => [
            'All anchors are linked correctly.',
        ],
        'tags' => [
          0 => [
              'name'  => 'a',
              'value' => 'Get your example here',
              'attributes' => [
                  'href'   => 'https://www.example.com',
                  'target' => '_blank',
                  'class'  => 'cta',
              ],
          ],
        ],
    ],
    'images' => [
        'notes' => [
            'All images seem to have either an alt attribute or a title attribute.',
        ],
        'tags' => [
          0 => [
              'name' => 'img',
              'attributes' => [
                  'src'   => 'https://www.example.com/image_01.jpg',
                  'alt'   => 'Image 01',
                  'class' => 'slider',
              ],
          ],
        ],
    ],
];
```

#### Further

See [`example/basic-usage.php`](example/basic-usage.php) to get an idea on how to render parsed tags.

## Docker

Set up a [docker](https://www.docker.com/) container on your local machine using [ddev](https://ddev.com).

#### Prerequisites

* Be sure to have [docker](https://www.docker.com/) installed and running. If not, follow [these instructions](https://www.docker.com/get-started).

#### Setup

Install ddev (using [Homebrew/Linuxbrew](https://ddev.readthedocs.io/en/stable/#homebrewlinuxbrew-macoslinux) is recommended):

```bash
$ brew tap drud/ddev && brew install ddev
```

Create and cd into your project directory (replace folder name ``<PATH-TO-YOUR-PROJECT-DIRECTORY>/my-project`` according to your needs):

```bash
$ mkdir -p <PATH-TO-YOUR-PROJECT-DIRECTORY>/my-project
$ cd <PATH-TO-YOUR-PROJECT-DIRECTORY>/my-project
```

#### Create a new docker container

Configure a ddev docker container (replace project-name `my-project` according to your needs):

```bash
$ ddev config --project-name=my-project --project-type=php --php-version=8.0 --docroot="" --create-docroot --disable-settings-management --http-port=8088 --https-port=44388
```

Create configured container and clone composer package into it:<br>
IMPORTANT: Eventually arising question ``Warning: ALL EXISTING CONTENT of the project root (~/my-project) will be deleted`` must be answered with 'yes'

```bash
$ ddev composer create oliver-noth/markup-check
```

Create index.php in document root of created container:

```bash
$ ddev exec chmod 755 example/write-index.sh && ddev exec ./example/write-index.sh
```

Finally start created container

```bash
$ ddev start
```

Once you have started the container, your project can be reached at ``http://my-project.ddev.site:8088``.<br>
NOTE: Subdomain `my-project` should be replaced with project-name you have chosen before.

#### Unit tests

This package is completely unit tested. You can run them in the created container:

```bash
$ ddev exec vendor/bin/phpunit
```

Displaying a code coverage summary needs enabling a code coverage driver. Use xdebug, check it and get the summary:<br>

```bash
$ ddev xdebug
$ ddev xdebug status
$ ddev exec vendor/bin/phpunit --coverage-text
```

NOTE: Although code coverage is at 100%, maybe the container displays a smaller coverage.<br>
Running it on your local machine (assuming a code coverage driver like xdebug is [installed](https://xdebug.org/docs/install)) in your project directory should display 100%. Working on a fix... 

```bash
$ vendor/bin/phpunit --coverage-text
```

#### Digging deeper

For instance managing the webserver behaviour of the container acting as such could be done by entering the container:

```bash
$ ddev ssh
```

MORE: Complete documentation can be found at [ddev.readthedocs.io]([https://ddev.readthedocs.io/)

## Authors

* [**Oliver Noth**](mailto:info@nothbetrieb.de)

