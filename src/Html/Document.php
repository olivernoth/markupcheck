<?php
declare(strict_types=1);
/**
 * Helper class for parsing an html document
 *   - Use class-methods suggested by IDE-Code-Completion
 *
 * @class HTMLDocument
 * @author Oliver Noth <info@nothbetrieb.de>
 * @copyright 2020 Oliver Noth
 */

namespace OliverNoth\MarkupCheck\Html;

use \DOMDocument, \DOMElement, \DOMAttr;
use \InvalidArgumentException;

/**
 * Class HTMLDocument
 *
 * @package OliverNoth\MarkupCheck\Helper
 * @since 1.0.0
 */
class Document
{
    /**
     * Holds markup. Initialized in self::__init().
     *
     * @see \OliverNoth\MarkupCheck\Html\Document::__init()
     * @since 1.0.0
     */
    protected ?string $markup = '';

    /**
     * Holds DOMDocument representing self::$markup. Initialized in self::__init().
     *
     * @see \OliverNoth\MarkupCheck\Html\Document::__init()
     * @since 1.0.0
     */
    protected ?DOMDocument $dom = null;

    /**
     * Getter for self::$markup.
     *
     * @return string Currently processed html markup
     * @see \OliverNoth\MarkupCheck\Html\Document::$markup
     * @since 1.0.0
     */
    public function getMarkup(): ?string
    {
        return $this->markup;
    }

    /**
     * Setter for self::$markup.
     *
     * @param string $markup html markup
     * @return $this $this (fluid mechanism: Returning $this provides method chaining)
     * @see \OliverNoth\MarkupCheck\Html\Document::$markup
     * @since 1.0.0
     */
    public function setMarkup(?string $markup): self
    {
        $this->markup = $markup;

        return $this;
    }

    /**
     * Getter for self::$dom.
     *
     * @return \DOMDocument|null DOMDocument object holding currently processed html markup
     * @see \OliverNoth\MarkupCheck\Html\Document::$dom
     * @since 1.0.0
     */
    public function getDom(): ?DOMDocument
    {
        return $this->dom;
    }

    /**
     * Setter for self::$dom.
     *
     * @param \DOMDocument|null $dom DOMDocument object holding currently processed html markup
     * @return $this $this (fluid mechanism: Returning $this provides method chaining)
     * @see \OliverNoth\MarkupCheck\Html\Document::$dom
     * @since 1.0.0
     */
    public function setDom(?DOMDocument $dom): self
    {
        $this->dom = $dom;

        return $this;
    }

    /**
     * HTMLDocument constructor.
     *
     * @param string $rawMarkup html markup to get parsed
     * @since 1.0.0
     */
    public function __construct(?string $rawMarkup)
    {
        $this->init($rawMarkup);
    }

    /**
     * Prepares object to get its methods properly working. Called in self::__construct.
     * Sets self::$markup and self::$dom.
     *
     * @param string $markup html markup to get parsed
     * @throws \InvalidArgumentException
     * @see   \OliverNoth\MarkupCheck\Html\Document::$dom
     * @since 1.0.0
     * @see   \OliverNoth\MarkupCheck\Html\Document::$markup
     */
    protected function init(?string $markup)
    {
        $this
            ->setMarkup($markup)
            ->setDom($this->getDOMDocument($markup))
        ;
    }

    /**
     * Simple check, if a given string contains any html markup.
     *
     * @param string $maybeHtmlMarkup String to validate
     * @return bool  Whether given string contains any html markup or not
     * @since 1.0.0
     */
    protected function containsHtmlMarkup(?string $maybeHtmlMarkup): bool
    {
        return is_string($maybeHtmlMarkup) && (!preg_match_all(
                '/<[^>]*>[^<>]*<\/[^>]*>/',
                $maybeHtmlMarkup,
                $matches
            ) ? false : true);
    }

    /**
     * Gets a new DOMDocument representing passed $markup.
     *
     * @param string $markup The html markup to get the DOMDocument object for
     * @return \DOMDocument  The DOMDocument object representing given html markup
     * @throws \InvalidArgumentException
     * @since 1.0.0
     */
    protected function getDOMDocument(?string $markup): DOMDocument
    {
        if (!$this->containsHtmlMarkup($markup)) {
            throw new InvalidArgumentException(!$markup ? 'Empty string provided' : 'Invalid markup provided');
        }

        libxml_use_internal_errors(true);

        $dom = new DOMDocument();

        $dom->encoding = 'utf-8';

        $dom->loadHTML($markup);

        return $dom;
    }

    /**
     * Gets an array representation of a passed DOMElement object.
     *
     * Sample return:
     * ```
     * [
     *     'name'  => 'a',
     *     'value' => 'Get your example here',
     *     'attributes' => [
     *         'href'   => 'https://www.example.com',
     *         'target' => '_blank',
     *         'class'  => 'cta',
     *     ],
     * ]
     * ```
     *
     * @param \DOMElement $element The DOMElement to get the array representation for
     * @return array Array representation of given DOMElement object
     * @since 1.0.0
     */
    protected function getDOMElementAsArray(?DOMElement $element): array
    {
        $attributes = [];

        /** @var DOMAttr $attribute */
        foreach ($element->attributes as $attribute) {
            $attributes[$attribute->nodeName] = $attribute->nodeValue;
        }

        $getElementValue = function(DOMElement $element) {
            // If appropriate, determine element's inner HTML and consider it as its node value
            if ($element->hasChildNodes() && false !== ($html = '')) {
                /** @var DOMElement $childNode */
                foreach ($element->childNodes as $childNode) {
                    $html .= $childNode->ownerDocument->saveHTML($childNode);
                }

                return $html;
            }

            return $element->nodeValue;
        };

        return [
            'name' => $element->nodeName,
            'value' => trim(preg_replace('/\n/', '', $getElementValue($element))),
            'attributes' => $attributes,
        ];
    }

    /**
     * Gets an array of tags specified by passed tag name.
     *
     * Sample return:
     * ```
     * [
     *     0 => [
     *         'name'  => 'a',
     *         'value' => 'Get your example here',
     *         'attributes' => [
     *             'href'   => 'https://www.example.com',
     *             'target' => '_blank',
     *             'class'  => 'cta',
     *         ],
     *     ],
     *     1 => [
     *         'name'  => 'a',
     *         'value' => 'Get your 2nd example here',
     *         'attributes' => [
     *             'href'   => 'https://www.example-two.com',
     *             'target' => '_blank',
     *             'class'  => 'cta',
     *         ],
     *     ],
     * ],
     * ```
     *
     * @param string $tag Specifies the tag name to get the tag array for
     * @return array An array of tags specified by given tag name
     * @since 1.0.0
     */
    public function getElementsByTagName(?string $tag): array
    {
        // Bail early if no string
        if (!is_string($tag)) {
            return [];
        }

        $elements = [];

        if (preg_match('/h[1-6]+/', $tag)) {
            return array_values(
                array_filter(
                    $this->getHeadings(),
                    function(array $heading) use ($tag) {
                        return isset($heading['name']) && $tag === $heading['name'];
                    }
                )
            );
        } else {
            /** @var DOMElement $element */
            foreach ($this->getDom()->getElementsByTagName($tag) as $element) {
                $elements[] = $this->getDOMElementAsArray($element);
            }
        }

        return $elements;
    }

    /**
     * Gets an array of h-tags.
     *
     * Sample return:
     * ```
     * [
     *     0 => [
     *         'name' => 'h1',
     *         'rank' => 1,
     *         'value' => 'First headline',
     *         'attributes' => [
     *             'class'  => 'article',
     *         ],
     *     ],
     *     1 => [
     *         'name' => 'h2',
     *         'rank' => 2,
     *         'value' => 'Second headline',
     *         'attributes' => [
     *             'class'  => 'unit',
     *         ],
     *     ],
     * ],
     * ```
     *
     * @return array An array of h-tags
     * @since 1.0.0
     */
    public function getHeadings(): array
    {
        $headings = [];

        preg_match_all('/<(h(\d))[^>]*>\s*([^<]*|<[^>]*>[^<]*<\/[^>]*>)\s*<\/h\d>/ism', $this->getMarkup(), $matches);

        foreach ($matches[0] as $key => $markup) {
            $dom = $this->getDOMDocument($markup);

            /** @var \DOMElement $htag */
            $htag = $dom->getElementsByTagName(strtolower($matches[1][$key]))->item(0);

            $htagElement = $this->getDOMElementAsArray($htag);

            $headings[] = [
                'name' => $htagElement['name'],
                'rank' => intval($matches[2][$key]),
                'value' => trim(preg_replace('/\n/', '', $matches[3][$key])),
                'attributes' => $htagElement['attributes'],
            ];
        }

        return $headings;
    }

    /**
     * Gets an array of a-tags (Shorthand for self::getElementsByTagName('a')).
     *
     * Sample return:
     * ```
     * [
     *     0 => [
     *         'name'  => 'a',
     *         'value' => 'Get your example here',
     *         'attributes' => [
     *             'href'   => 'https://www.example.com',
     *             'target' => '_blank',
     *             'class'  => 'cta',
     *         ],
     *     ],
     *     1 => [
     *         'name'  => 'a',
     *         'value' => 'Get your 2nd example here',
     *         'attributes' => [
     *             'href'   => 'https://www.example-two.com',
     *             'target' => '_blank',
     *             'class'  => 'cta',
     *         ],
     *     ],
     * ],
     * ```
     *
     * @return array An array of a-tags
     * @see \OliverNoth\MarkupCheck\Html\Document::getElementsByTagName()
     * @since 1.0.0
     */
    public function getAnchors(): array
    {
        return $this->getElementsByTagName('a');
    }

    /**
     * Gets an array of img-tags.
     *
     * Sample return:
     * ```
     * [
     *     0 => [
     *         'name' => 'img',
     *         'attributes' => [
     *             'src'   => 'https://www.example.com/image_01.jpg',
     *             'alt'   => 'Image 01',
     *             'class' => 'slider',
     *         ],
     *     ],
     *     1 => [
     *         'name' => 'img',
     *         'attributes' => [
     *             'src'   => 'https://www.example.com/image_02.jpg',
     *             'alt'   => 'Image 02',
     *             'class' => 'gallery',
     *         ],
     *     ],
     * ],
     * ```
     *
     * @return array An array of img-tags
     * @see \OliverNoth\MarkupCheck\Html\Document::getElementsByTagName()
     * @since 1.0.0
     */
    public function getImages(): array
    {
        // Remove key 'value' on each found img element
        return array_map(
            function($image) {
                return array_filter(
                    $image,
                    function($key) {
                        return 'value' !== $key;
                    },
                    ARRAY_FILTER_USE_KEY
                );
            },
            $this->getElementsByTagName('img')
        );
    }
}
