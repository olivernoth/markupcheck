<?php

declare(strict_types=1);
/**
 * Entry point to MarkupCheck\Html
 *   - Acts as a director: Orchestrates html markup check functionality
 *   - Parses an html markup given by either an url to curl the markup from or the markup itself
 *   - Provides parse results
 *   - Use class-methods suggested by IDE-Code-Completion
 *
 * @class Director
 * @author Oliver Noth <info@nothbetrieb.de>
 * @copyright 2020 Oliver Noth
 */

namespace OliverNoth\MarkupCheck\Html;

use \OliverNoth\MarkupCheck\Helper\{Url, Utils};
use \Exception;

/**
 * Class Director
 *
 * @package OliverNoth\MarkupCheck
 * @since 1.0.0
 */
class Director
{
    /**
     * Constants used for determined processing context. Used for self::$context.
     *
     * @see \OliverNoth\MarkupCheck\Html\Director::$context
     * @since 1.0.0
     */
    protected const CONTEXT_NO_ERROR = 1;
    protected const CONTEXT_ERROR = 2;
    protected const CONTEXT_URL_GIVEN = 4;
    protected const CONTEXT_MARKUP_GIVEN = 8;
    protected const CONTEXT_NEITHER_URL_NOR_MARKUP_GIVEN = 16;

    /**
     * Constants used for determined heading notes. Used in self::getHeadingNotes().
     *
     * @see \OliverNoth\MarkupCheck\Html\Director::getHeadingNotes()
     * @since 1.0.0
     */
    protected const NO_H1_TAG = 'Markup does not contain a first heading (h1 tag).';
    protected const MORE_THAN_ONE_H1_TAG = 'Markup contains %d first headings (h1 tag). Should be exactly one.';
    protected const EXACTLY_ONE_H1_TAG = 'Markup contains exactly 1 first heading (h1 tag).';
    protected const H_TAGS_IN_RIGHT_ORDER = 'All headings seem to be in right order.';
    protected const H_TAGS_NOT_IN_RIGHT_ORDER = 'Headings contained in markup are not in the right order.';

    /**
     * Constants used for determined anchor notes. Used in self::getAnchorNotes().
     *
     * @see \OliverNoth\MarkupCheck\Html\Director::getAnchorNotes()
     * @since 1.0.0
     */
    protected const MORE_THAN_ONE_INTERNAL_ABSOLUTE_A_TAG = 'One or more internal link is absolute and should be relative.';
    protected const ALL_A_TAGS_LINKED_CORRECTLY = 'All anchors are linked correctly.';

    /**
     * Constants used for determined image notes. Used in self::getImageNotes().
     *
     * @see \OliverNoth\MarkupCheck\Html\Director::getImageNotes()
     * @since 1.0.0
     */
    protected const ONE_OR_MORE_IMG_TAG_NOT_COMPLETE = 'One or more img tags has neither an alt attribute nor a title attribute.';
    protected const ALL_IMG_TAGS_COMPLETE = 'All images seem to have either an alt attribute or a title attribute.';

    /**
     * Bitmask for determined processing context. Initialized in self::init().
     *
     * @see \OliverNoth\MarkupCheck\Html\Director::init()
     * @since 1.0.0
     */
    protected int $context = Director::CONTEXT_NO_ERROR;

    /**
     * Holds constructor param. Initialized in self::init().
     *
     * @see \OliverNoth\MarkupCheck\Html\Director::init()
     * @since 1.0.0
     */
    protected string $maybeUri = '';

    /**
     * Holds Url object to get the markup from. Initialized in self::init().
     *
     * @see \OliverNoth\MarkupCheck\Html\Director::init()
     * @since 1.0.0
     */
    protected ?Url $url = null;

    /**
     * Holds HTMLDocument representing currently processed markup. Initialized in self::init().
     *
     * @see \OliverNoth\MarkupCheck\Html\Director::init()
     * @since 1.0.0
     */
    protected ?Document $htmlDocument = null;

    /**
     * Holds exception. Updated in self::init() in case an error occurred.
     *
     * @see \OliverNoth\MarkupCheck\Html\Director::init()
     * @since 1.0.0
     */
    protected ?array $exceptions = null;

    /**
     * Setter for self::$url.
     *
     * @param Url|null $url Url object to get the markup from
     * @return $this $this (fluid mechanism: Returning $this provides method chaining)
     * @see \OliverNoth\MarkupCheck\Html\Director::$url
     * @since 1.0.0
     */
    public function setUrl(?Url $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Setter for self::htmlDocument.
     *
     * @param Document|null $htmlDocument HTMLDocument representing currently processed markup
     * @return $this $this (fluid mechanism: Returning $this provides method chaining)
     * @see Director::$htmlDocument
     */
    public function setHtmlDocument(?Document $htmlDocument): self
    {
        $this->htmlDocument = $htmlDocument;

        return $this;
    }

    /**
     * Director constructor.
     *
     * @param string $maybeUri Url to get the markup from.
     *                         Will be url validated and in case of failure considered as markup.
     * @since 1.0.0
     */
    public function __construct(string $maybeUri = '')
    {
        $this
            ->setUrl(new Url($maybeUri))
            ->init($maybeUri)
        ;
    }

    /**
     * Prepares object to get its methods properly working. Called in self::__construct.
     * Sets self::$url and self::$htmlDocument on success, self::$exceptions on failure.
     * Sets also self::$maybeUrl and self::$context.
     *
     * @param string $maybeUri Url to get the markup from.
     *                         Will be url validated and in case of failure considered as markup.
     * @see \OliverNoth\MarkupCheck\Html\Director::__construct()
     * @see \OliverNoth\MarkupCheck\Html\Director::$maybeUri
     * @see \OliverNoth\MarkupCheck\Html\Director::$url
     * @see \OliverNoth\MarkupCheck\Html\Director::$htmlDocument
     * @see \OliverNoth\MarkupCheck\Html\Director::$exceptions
     * @since 1.0.0
     */
    protected function init(string $maybeUri = '')
    {
        $this->maybeUri = $maybeUri;

        if ($this->url && (string)$this->url) {
            try {
                $this->setHtmlDocument(new Document($this->url->determineMarkup()));

                $this->context = static::CONTEXT_NO_ERROR | static::CONTEXT_URL_GIVEN;
            } catch (Exception $exception) {
                $this->exceptions[] = $exception;

                $this->context = static::CONTEXT_ERROR | static::CONTEXT_URL_GIVEN;
            }
        } else {
            try {
                $this->setHtmlDocument(new Document($maybeUri));

                $this->context = static::CONTEXT_NO_ERROR | static::CONTEXT_MARKUP_GIVEN;
            } catch (Exception $exception) {
                $this->exceptions = [
                    $exception,
                    new Exception('Neither a valid url nor a valid markup has been provided'),
                ];

                $this->context = static::CONTEXT_ERROR | static::CONTEXT_NEITHER_URL_NOR_MARKUP_GIVEN;
            }
        }
    }

    /**
     * Checks given h tag elements and returns check results in an array.
     * Sample return:
     * ```
     * [
     *   'Markup does not contain a first heading (h1 tag).',
     * ]
     * ```
     *
     * @param array $hTagElements h-Tags in an array given by HTMLDocument::getHeadings()
     * @return array Determined check results
     * @see \OliverNoth\MarkupCheck\Html\Document::getHeadings()
     * @since 1.0.0
     */
    protected function getHeadingNotes(?array $hTagElements = []): array
    {
        // Bail early if no tag elements
        if (empty($hTagElements)) {
            return [];
        }

        $notes = [];

        $firstRankedHeadings = array_filter(
            $hTagElements,
            function(array $heading) {
                return !empty($heading['rank']) && 1 === intval($heading['rank']);
            }
        );

        if (empty($firstRankedHeadings)) {
            $notes[] = static::NO_H1_TAG;
        } elseif (false !== ($firstHeadingCount = count($firstRankedHeadings)) && 1 < $firstHeadingCount) {
            $notes[] = sprintf(static::MORE_THAN_ONE_H1_TAG, $firstHeadingCount);
        } else {
            $notes[] = static::EXACTLY_ONE_H1_TAG;
        }

        $orderValid = true;
        for ($headingIndex = 0; count($hTagElements) > $headingIndex; $headingIndex++) {
            if (0 < $headingIndex && $hTagElements[$headingIndex]['rank'] < $hTagElements[$headingIndex - 1]['rank']) {
                $orderValid = false;
                break;
            }
        }

        if ($orderValid) {
            $notes[] = static::H_TAGS_IN_RIGHT_ORDER;
        } else {
            $notes[] = static::H_TAGS_NOT_IN_RIGHT_ORDER;
        }

        return $notes;
    }

    /**
     * Checks given a tag elements and returns check results in an array.
     * Sample return:
     * ```
     * [
     *   'One or more internal links is absolute and should be relative.',
     * ]
     * ```
     *
     * @param array $aTagElements a-Tags in an array given by HTMLDocument::getAnchors()
     * @return array Determined check results
     * @see \OliverNoth\MarkupCheck\Html\Document::getAnchors()
     * @since 1.0.0
     */
    protected function getAnchorNotes(?array $aTagElements = []): array
    {
        // Bail early if no tag elements
        if (empty($aTagElements)) {
            return [];
        }

        $absoluteLinks = array_map(
            function($aTag) {
                return $aTag['attributes']['href'];
            },
            array_filter(
                $aTagElements,
                function($aTag) {
                    return !empty($aTag['attributes']['href']) && preg_match(
                            '/^http[s]{0,1}/',
                            $aTag['attributes']['href']
                        );
                }
            )
        );

        if (!empty($absoluteLinks) && false !== ($host = $this->url->host) && !empty($host)) {
            $host = trim($host, '/');

            $absoluteInternalLinks = array_filter(
                $absoluteLinks,
                function($link) use ($host) {
                    return preg_match("/$host/", $link);
                }
            );

            if (!empty($absoluteInternalLinks)) {
                return [static::MORE_THAN_ONE_INTERNAL_ABSOLUTE_A_TAG];
            }
        }

        return [static::ALL_A_TAGS_LINKED_CORRECTLY];
    }

    /**
     * Checks given img tag elements and returns check results in an array.
     * Sample return:
     * ```
     * [
     *   'One or more img tags has neither an alt attribute nor a title attribute.',
     * ]
     * ```
     *
     * @param array $imgTagElements img-Tags in an array given by HTMLDocument::getImages()
     * @return array Determined check results
     * @see \OliverNoth\MarkupCheck\Html\Document::getImages()
     * @since 1.0.0
     */
    protected function getImageNotes(?array $imgTagElements = []): array
    {
        // Bail early if no tag elements
        if (empty($imgTagElements)) {
            return [];
        }

        $notCompleteImgTags = array_filter(
            $imgTagElements,
            function($imgTag) {
                return empty($imgTag['attributes']['alt']) && empty($imgTag['attributes']['title']);
            }
        );

        if (!empty($notCompleteImgTags)) {
            return [static::ONE_OR_MORE_IMG_TAG_NOT_COMPLETE];
        }

        return [static::ALL_IMG_TAGS_COMPLETE];
    }

    /**
     * Gets parsed results depending on self::$markup.
     * Sample return in case of failure:
     * ```
     * [
     *     'errors' => [
     *         'notes' => [
     *             'Neither a valid url nor a valid markup has been provided',
     *         ],
     *         'tags' => [],
     *     ],
     * ]
     * ```
     * Sample return in case of success:
     * ```
     * [
     *     'headings' => [
     *         'notes' => [
     *             'Markup contains exactly 1 first heading (h1 tag).',
     *             'All headings seem to be in right order.',
     *         ],
     *         'tags' => [
     *             0 => [
     *                 'name'  => 'h1',
     *                 'rank'  => 1,
     *                 'value' => 'First headline',
     *                 'attributes' => [
     *                     'class'  => 'article',
     *                 ],
     *             ],
     *             1 => [
     *                 'name'  => 'h2',
     *                 'rank'  => 2,
     *                 'value' => 'Second headline',
     *                 'attributes' => [
     *                     'class'  => 'unit',
     *                 ],
     *             ],
     *         ],
     *     ],
     *     'anchors' => [
     *         'notes' => [
     *             'All anchors are linked correctly.',
     *         ],
     *         'tags' => [
     *             0 => [
     *                 'name'  => 'a',
     *                 'value' => 'Get your example here',
     *                 'attributes' => [
     *                     'href'   => 'https://www.example.com',
     *                     'target' => '_blank',
     *                     'class'  => 'cta',
     *                 ],
     *             ],
     *             1 => [
     *                 'name'  => 'a',
     *                 'value' => 'Get your 2nd example here',
     *                 'attributes' => [
     *                     'href'   => 'https://www.example-two.com',
     *                     'target' => '_blank',
     *                     'class'  => 'cta',
     *                 ],
     *             ],
     *         ],
     *     ],
     *     'images' => [
     *         'notes' => [
     *             'All images seem to have either an alt attribute or a title attribute.',
     *         ],
     *         'tags' => [
     *           0 => [
     *               'name' => 'img',
     *               'attributes' => [
     *                   'src'   => 'https://www.example.com/image_01.jpg',
     *                   'alt'   => 'Image 01',
     *                   'class' => 'slider',
     *               ],
     *           ],
     *           1 => [
     *               'name' => 'img',
     *               'attributes' => [
     *                   'src'   => 'https://www.example.com/image_02.jpg',
     *                   'alt'   => 'Image 02',
     *                   'class' => 'gallery',
     *               ],
     *           ],
     *         ],
     *     ],
     * ]
     * ```
     *
     * @return array Determined parsed results
     * @see \OliverNoth\MarkupCheck\Html\Director::$markup
     * @since 1.0.0
     */
    public function getParsedTags(): array
    {
        if (!$this->exceptions) {
            $headings = $this->htmlDocument->getHeadings();
            $anchors = $this->htmlDocument->getAnchors();
            $images = $this->htmlDocument->getImages();

            return [
                'headings' => [
                    'notes' => $this->getHeadingNotes($headings),
                    'tags' => $headings,
                ],
                'anchors' => [
                    'notes' => $this->getAnchorNotes($anchors),
                    'tags' => $anchors,
                ],
                'images' => [
                    'notes' => $this->getImageNotes($images),
                    'tags' => $images,
                ],
            ];
        }

        return [
            'errors' => [
                'notes' => array_map(
                    function(Exception $exception) {
                        return rtrim($exception->getMessage(), '.') . '.';
                    },
                    $this->exceptions
                ),
                'tags' => [],
            ],
        ];
    }

    /**
     * Renders parsed results depending on self::$markup.
     *
     * To avoid a JSON parse error a call to this method should be followed by an exit call.
     *
     * @return void Renders determined parsed results as json
     * @see \OliverNoth\MarkupCheck\Html\Director::getParsedTags()
     * @since 1.0.0
     */
    public function renderParsedTagsAsJson(): void
    {
        header('Content-Type: application/json');
        echo json_encode($this->getParsedTags());
    }

    /**
     * Gets the label depending on passed constructor param, which lives in self::$maybeUrl.
     * If a markup instead of an url has been passed to the constructor, an excerpt of the markup will be returned.
     * Sample return for constructor param <code>'example.com'</code>:
     * ```
     * 'Given uri "https://example.com"'
     * ```
     * Sample return for constructor param <code>'<h1>First headline</h1><p>Lorem ipsum dolor sit amet</p>'</code>:
     * ```
     * 'Given markup "<h1>First headline</h1><p>Lorem ipsum dolor..."'
     * ```
     * Sample return for constructor param <code>'Lorem ipsum dolor sit amet'</code>:
     * ```
     * 'Lorem ipsum dolor sit amet (errors occurred)'
     * ```
     *
     * @return string
     * @see \OliverNoth\MarkupCheck\Html\Director::$maybeUri
     * @since 1.0.0
     */
    public function getLabel(): string
    {
        $label = '';

        if (static::CONTEXT_URL_GIVEN & $this->context) {
            $label .= sprintf('Given uri "%s"', $this->maybeUri);
        } elseif (static::CONTEXT_MARKUP_GIVEN & $this->context) {
            $label .= sprintf('Given markup "%s"', Utils::excerpt($this->maybeUri, 80));
        } elseif (static::CONTEXT_NEITHER_URL_NOR_MARKUP_GIVEN & $this->context) {
            $label .= Utils::excerpt($this->maybeUri, 80);
        }

        if (static::CONTEXT_ERROR & $this->context) {
            $label .= ' (errors occurred)';
        }

        return trim($label);
    }
}