<?php
declare(strict_types=1);
/**
 * Entry point to MarkupCheck
 *   - Acts as a factory: Creates MarkupChecker for a given markup type
 *   - To add more markup types factory should be subclassed
 *   - Use class-methods suggested by IDE-Code-Completion
 *
 * @class Factory
 * @author Oliver Noth <info@nothbetrieb.de>
 * @copyright 2020 Oliver Noth
 */

namespace OliverNoth\MarkupCheck\Main;

use \OliverNoth\MarkupCheck\Html\Director as HtmlDirector;

/**
 * Class Factory
 *
 * @package OliverNoth\MarkupCheck
 * @since 1.0.0
 */
class Factory
{
    /**
     * Creates a new instance of \OliverNoth\MarkupCheck\Html\Director, which then can be used to
     * parse the markup and get the parsed results.
     *
     * @param string $maybeUrl Url to get the markup from or the markup itself
     * @return HtmlDirector
     */
    public function createHtmlMarkupChecker(string $maybeUrl): HtmlDirector
    {
        return new HtmlDirector($maybeUrl);
    }
}