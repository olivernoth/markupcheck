<?php
declare(strict_types=1);
/**
 * Helper class for providing an url object
 *   - Use class-methods suggested by IDE-Code-Completion
 *
 * @class Url
 * @author Oliver Noth <info@nothbetrieb.de>
 * @copyright 2020 Oliver Noth
 */

namespace OliverNoth\MarkupCheck\Helper;

use \Exception;

/**
 * Class Url
 *
 * @package OliverNoth\MarkupCheck\Helper
 * @property-read string $uri
 * @property-read string $scheme
 * @property-read string $host
 * @property-read string $port
 * @property-read string $path
 * @property-read string $user
 * @property-read string $pass
 * @property-read string $query
 * @since 1.0.0
 */
class Url
{
    /**
     * Holds read-only properties.
     *
     * @since 1.0.0
     */
    protected array $properties = [
        'uri' => '',
        'scheme' => '',
        'host' => '',
        'port' => '',
        'path' => '',
        'user' => '',
        'pass' => '',
        'query' => '',
    ];

    /**
     * Magic getter for class instance property.
     *
     * Returns found string property on success, an empty string on failure.
     *
     * @param string $property
     * @return string|null
     * @since 1.0.0
     */
    public function __get(string $property)
    {
        return $this->properties[$property] ?? null;
    }

    /**
     * When casted to scalar string, self::$uri will be returned.
     *
     * @return string
     * @see \OliverNoth\MarkupCheck\Helper\Url::$uri
     * @since 1.0.0
     */
    public function __toString(): string
    {
        return $this->uri;
    }

    /**
     * Url constructor.
     *
     * If an Url class instance is casted to scalar string, the determined uri will be returned:
     *
     * Samples:
     * ```
     * (string) (new Url('www.example.com')) leads to 'http://www.example.com'
     * (string) (new Url('example.com/index.php')) leads to 'https://example.com/index.php'
     * (string) (new Url('example.com/users/?id=2')) leads to 'https://example.com/users/?id=2'
     * (string) (new Url('user:pass@example.com/')) leads to 'https://user:pass@example.com/'
     * ```
     *
     * @param string $maybeUrl
     * @since 1.0.0
     */
    public function __construct(?string $maybeUrl)
    {
        $this->init($maybeUrl);
    }

    /**
     * Prepares object to get its methods properly working. Called in self::__construct.
     *
     * Checks, if passed string is a well-formed uri.
     *
     * Passed string will be considered as a well-formed uri, if it contains one top-level-domain and at least one sub-level-domain.
     * If a scheme is not found, 'https' will be assumed.
     *
     * @param string $maybeUrl
     * @since 1.0.0
     */
    protected function init(?string $maybeUrl)
    {
        // Before parsing url, ensure that given string is a well-formed uri
        if (is_string($maybeUrl) &&
            preg_match(
                '/^((http[s]{0,1}:\/\/|[\/]{2,})([\w]+:[\w]+@)*)*([\w\-_]+\.[\w\-_]+|[\w\-_]+(:[\d]{2,4})+)(\.[\w\-_]+)*(\/[\w\-_]+(\.[a-z]+)*)*[\/]*[?&=\w]*$/i',
                $maybeUrl
            ) &&
            false !== ($parsedResult = parse_url($maybeUrl))
        ) {
            if (empty($parsedResult['host']) && !empty($parsedResult['path'])) {
                $parts = explode('/', trim($parsedResult['path'], '/'));

                $parsedResult['host'] = array_shift($parts);
                $parsedResult['path'] = '/' . implode('/', $parts);
            }

            foreach (['path', 'query'] as $resultKey) {
                if (!empty($parsedResult[$resultKey])) {
                    $parsedResult[$resultKey] = '/' . ('query' === $resultKey ? '?' : '') . trim(
                            $parsedResult[$resultKey],
                            '/?'
                        );
                }
            }

            // preg_match above ensures, that following determinations can be done the simple way
            $this->properties = array_merge(
                $this->properties,
                [
                    'scheme' => $parsedResult['scheme'] ?? 'https',
                    'host' => $parsedResult['host'] ?? '',
                    'port' => $parsedResult['port'] ?? '',
                    'path' => $parsedResult['path'] ?? '',
                    'user' => $parsedResult['user'] ?? '',
                    'pass' => $parsedResult['pass'] ?? '',
                    'query' => $parsedResult['query'] ?? '',
                ]
            );

            /**
             * Setting uri must be done afterwards, because it depends on accessing $this->[property] (uses magic self::__get)
             * @see \OliverNoth\MarkupCheck\Helper\Url::__get()
             */
            $this->properties['uri'] =
                $this->scheme . '://' .
                trim(
                    ($this->user ? $this->user . ':' : '') . ($this->pass ? $this->pass . '@' : '') .
                    $this->host .
                    ($this->port ? ':' . $this->port : '') .
                    $this->path . $this->query,
                    '/'
                ) .
                (!$this->query ? '/' : '');
        }
    }

    /**
     * Curls markup depending on self::$uri and returns it.
     *
     * @return string Determined html markup
     * @throws \Exception
     * @since 1.0.0
     * @see \OliverNoth\MarkupCheck\Helper\Url::$uri
     */
    public function determineMarkup(): string
    {
        $curlResource = curl_init($this->uri);

        // Return the transfer as a string
        curl_setopt($curlResource, CURLOPT_RETURNTRANSFER, true);

        $markup = curl_exec($curlResource);

        if (curl_errno($curlResource)) {
            throw new Exception(curl_error($curlResource));
        }

        curl_close($curlResource);

        return $markup;
    }
}